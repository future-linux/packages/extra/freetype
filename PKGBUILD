# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=freetype
pkgver=2.12.1
pkgrel=2
pkgdesc="Font rasterization library"
arch=('x86_64')
url="https://www.freetype.org/"
license=('GPL')
depends=('zlib' 'bzip2' 'bash' 'libpng' 'harfbuzz' 'brotli' 'which')
makedepends=('libx11')
backup=(etc/profile.d/freetype2.sh)
install=${pkgname}.install
source=(https://downloads.sourceforge.net/${pkgname}/${pkgname}-${pkgver}.tar.xz
    freetype.sh)
sha256sums=(4766f20157cc4cf0cd292f80bf917f92d1c439b243ac3018debf6b9140c41a7f
    f7f8e09c44f7552c883846e9a6a1efc50377c4932234e74adc4a8ff750606467)

prepare() {
    cd ${pkgname}-${pkgver}

    sed -ri "s:.*(AUX_MODULES.*valid):\1:" modules.cfg

    sed -r "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:" \
        -i include/freetype/config/ftoption.h
}

build() {
    cd ${pkgname}-${pkgver}

    ${configure}                  \
        --enable-freetype-config  \
        --disable-static

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    install -Dt ${pkgdir}/etc/profile.d -m644 ${srcdir}/freetype.sh

    install -Dt ${pkgdir}/usr/share/aclocal -m644 builds/unix/freetype2.m4
}
